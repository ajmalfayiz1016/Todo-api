from django.urls import paths
from api.v1.tasks import views


urlpatterns = [
    path('',views.tasks)
]